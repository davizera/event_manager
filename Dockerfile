FROM node:alpine

RUN mkdir /home/app

RUN npm install nodemon -g

WORKDIR /home/app

ADD package.json /home/app

RUN npm install

ADD . /home/app
