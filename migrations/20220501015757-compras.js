'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     */
    await queryInterface.createTable('compras', 
      { 
        id: {
          type:Sequelize.INTEGER,
          primaryKey: true,
          allowNull:false,
          autoIncrement: true
        },
        id_usuario:{
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'usuarios',
            key: 'id'
          }
        },
        valor: {
          type: Sequelize.FLOAT,
          allowNull: false
        },
        quantidade: {
          type: Sequelize.INTEGER,
          allowNull: false
        }
      }
    );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     */
    await queryInterface.dropTable('compras');
  }
};
