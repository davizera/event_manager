'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     */
    await queryInterface.createTable('eventos', 
      { 
        id: {
          type:Sequelize.INTEGER,
          primaryKey: true,
          allowNull:false,
          autoIncrement: true
        },
        descricao: {
          type: Sequelize.STRING,
          allowNull: false
        },
        nome: {
          type: Sequelize.STRING,
          allowNull: false
        },
        data:{ 
          type: Sequelize.DATE,
          allowNull: false
        },
        imagem: {
          type: Sequelize.BLOB,
          allowNull: true,
          defaultValue: null
        },
        privado: {
          allowNull: true,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        },
        endereco: {
          type: Sequelize.STRING,
          allowNull: false
        },
        id_usuario: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'usuarios',
            key: 'id'
          }
        },
        valor: {
          allowNull: true,
          defaultValue: null,
          type: Sequelize.FLOAT
        },
        gratuito: {
          allowNull: true,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }
    );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     */
    await queryInterface.dropTable('eventos');
  }
};
