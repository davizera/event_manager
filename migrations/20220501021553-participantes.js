'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     */
    await queryInterface.createTable('participantes',
     { 
      id: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        allowNull:false,
        autoIncrement: true
      },
      id_ingresso: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'ingressos',
          key: 'id'
        }
      },
      id_evento:{
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'eventos',
          key: 'id'
        }
      },
      id_usuario:{
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'usuarios',
          key: 'id'
        }
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false
      }
     }
    );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     */
    await queryInterface.dropTable('participantes');
  }
};
