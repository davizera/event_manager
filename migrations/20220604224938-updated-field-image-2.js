module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     */
    await queryInterface.changeColumn("eventos", "imagem", {
      type: Sequelize.BLOB("long"),
      allowNull: true,
      defaultValue: null,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     */
    await queryInterface.removeColumn("eventos", "imagem", {
      type: Sequelize.BLOB("long"),
      allowNull: true,
      defaultValue: null,
    });
  },
};
