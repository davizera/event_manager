const User = require("../models/Usuario");
const Evento = require("../models/Evento");
const { Op } = require('sequelize');
const multer = require("multer");
require("dotenv").config();
const validate = require("../utils/validation")();
module.exports = {
  async createEvent(req, res) {
    const payload = { ...req.body };
    if (req.file) {
      payload.imagem = req.file;
    }
    const findUser = await User.findByPk(req.body.id_usuario);
    try {
      validate.existsOrError(payload.descricao, "Descrição não informado");
      validate.existsOrError(payload.nome, "nome não informado");
      validate.existsOrError(payload.data, "data não informada");
      validate.existsOrError(payload.endereco, "endereço não informado");
      if (findUser) {
        payload.id_usuario = req.body.id_usuario;
        const createdEvent = await Evento.create({ ...payload });
        if (createdEvent) return res.json(createdEvent);
      }
      return res.status(404).send("User not found");
    } catch (msg) {
      console.log(msg)
      return res.status(500).send(msg);
    }
  },
  async editEvent(req, res) {
    const payload = { ...req.body };
    if (req.file) {
      payload.imagem = req.file;
    }
    console.log(payload)
    const findEvent = await Evento.findByPk(req.params.id_evento);
    try {
      validate.existsOrError(payload.descricao, "Descrição não informado");
      validate.existsOrError(payload.nome, "nome não informado");
      validate.existsOrError(payload.data, "data não informada");
      validate.existsOrError(payload.endereco, "endereço não informado");
      if (findEvent) {
        const updatedEvent = await findEvent.update({ ...req.body });
        if (updatedEvent) return res.json(updatedEvent);
      }
      return res.status(404).send("Event not found");
    } catch (msg) {
      console.log(msg)
      return res.status(500).send(msg);
    }
  },
  async getEvents(req, res) {
    try {
      const events = await Evento.findAll();
      if (events) {
        return res.json(events);
      }
    } catch (msg) {
      return res.status(500).send(msg);
    }
  },
  async getEventByUserId(req, res) {
    try {
      const findEvent = await Evento.findAll({
        where: { id_usuario: req.params.id_usuario },
      });
      if (findEvent.length > 0) {
        return res.status(200).send(findEvent);
      }
      return res.status(404).send("event not found");
    } catch (msg) {
      return res.status(500).send(msg);
    }
  },
  async getEventById(req, res) {
    try {
      console.log(req.params.id_event)
      const findEvent = await Evento.findOne({
        where: { id: parseInt(req.params.id_event) },
      });
      if (findEvent) {
        return res.status(200).send(findEvent);
      }
      return res.status(404).send("event not found");
    } catch (msg) {
      return res.status(500).send(msg);
    }
  },
  async deleteEventById(req, res) {
    try {
      const findEvent = await Evento.findAll({
        where: { id: req.params.id_evento },
      });
      if (findEvent.length > 0) {
        await Evento.destroy({ where: { id: req.params.id_evento } });
        return res.status(200).send("event deleted with sucess");
      }
      return res.status(404).send("event not found");
    } catch (msg) {
      return res.status(500).send(msg);
    }
  },
  async searchEvent(req, res) {
    try {
      const findEvent = await Evento.findAll({
        where: {
          nome: { 
            [Op.like]: '%' + req.body.query + '%'
          }
        }
      });
      if (findEvent.length > 0) {
        return res.status(200).send(findEvent);
      }
      return res.status(404).send("event not found");
    } catch (msg) {
      return res.status(500).send(msg);
    }
  }
};
