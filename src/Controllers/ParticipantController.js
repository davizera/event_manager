const User = require("../models/Usuario");
const Evento = require("../models/Evento");
const Participante = require("../models/Participante");
require("dotenv").config();
const validate = require("../utils/validation")();
module.exports = {
  async createParticipant(req, res) {
    const payload = { ...req.body };
    const findEvent = await Evento.findByPk(req.params.id_evento);
    const findUser = await User.findByPk(req.params.id_usuario)
    if (!findEvent)  return res.status(404).send('Event not found')
    if (!findUser) return res.status(404).send('User not found')
    try {
      validate.existsOrError(payload.nome, "nome não informado");
      validate.existsOrError(req.params.id_usuario, "id usuario não informado");
      validate.existsOrError(req.params.id_evento, "id evento não informado");
      if (findEvent && findUser) {
        payload.id_usuario = req.params.id_usuario
        payload.id_evento = req.params.id_evento
        const createdParticpant = await Participante.create({ ...payload });
        if (createdParticpant) return res.json(createdParticpant);
      }
    } catch (msg) {
      return res.status(500).send(msg);
    }
  },
  async deleteParticipantesByEventId(req, res) {
    // try {
    //   const events = await Evento.findAll()
    //   if (events) {
    //     return res.json(events)
    //   }
    // } catch (msg) {
    //   return res.status(500).send(msg);
    // }
  },
  async getParticipantes(req, res) {
    try {
      const participants = await Participante.findAll()
      if (participants) {
        return res.json(participants)
      }
    } catch (msg) {
      return res.status(500).send(msg);
    }
  },
};
