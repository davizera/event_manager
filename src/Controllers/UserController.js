const User = require("../models/Usuario");
require("dotenv").config();
const jwt = require("jwt-simple");
const validate = require("../utils/validation")();
const bcrypt = require("bcrypt");
const encryptPassword = (password) => {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
};
module.exports = {
  async Register(req, res) {
    const payload = { ...req.body };
    try {
      validate.existsOrError(payload.nome, "Nome não informado");
      validate.existsOrError(payload.email, "E-mail não informado");
      validate.existsOrError(payload.password, "Senha não informada");
      const userExist = await User.findAll({ where: { email: payload.email } });
      if (userExist) {
        validate.notExistsOrError(userExist, "Usuário já cadastrado");
      }
    } catch (msg) {
      console.log(msg);
      return res.status(400).send(msg);
    }
    payload.password = encryptPassword(payload.password);
    const createdUser = await User.create({ ...payload });
    return res.json(createdUser);
  },
  // async List(_,res) {
  //   const users = await User.findAll()
  //   return res.json(users)
  // },
  async Login(req, res) {
    if (!req.body.email || !req.body.password) {
      return res.status(400).send("Informe usuário e senha!");
    }
    console.log(req.body)
    const [user] = await User.findAll({
      attributes: ["email", "password","nome", "id", "admin"],
      where: { email: req.body.email },
    });
    if (!user) return res.status(400).send("Usuário não encontrado!");
    let isMatch = await bcrypt.compare(
      req.body.password,
      user.dataValues.password
    );
    if (!isMatch) return res.status(401).send("Email/Senha inválidos!");
    const now = Math.floor(Date.now() / 1000);
    const payload = {
      id: user.id,
      name: user.nome,
      email: user.email,
      admin: user.admin ? user.admin : false,
      iat: now,
      exp: now + 60 * 60 * 24 * 3, // token válido por 3dias
    };
    res.json({
      // ...payload,
      token: jwt.encode(payload, process.env.AUTHSECRET),
    });
  },
  // async Edit(req,res) {
  //   if (!req.body.email || !req.body.password) {
  //     return res.status(400).send('Informe usuário e senha!')
  //   }
  //   const findUser = await User.findByPk(req.params.id)
  //   if (!findUser) {
  //     return res.status(404).send({error: 'user not found'})
  //   }
  //   const updatedUser = {
  //     email: req.body.email,
  //     password: encryptPassword(req.body.password)
  //   }
  //   try {
  //     await User.update(
  //       { ...updatedUser },
  //       { where: { id: req.params.id }}
  //   )
  //     return res.json({status : 'Usuário editado com sucesso!'});
  //   } catch (error) {
  //     return res.status(500).send(error)
  //   }
  // }
};
