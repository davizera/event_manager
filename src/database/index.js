require('dotenv').config()
const Sequelize = require("sequelize");
const { DB_NAME, DB_PASSWORD, DB_DIALECT, DB_HOST, DB_USERNAME} = process.env
const connection = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, { host: DB_HOST, dialect: DB_DIALECT, define: {
  timestamps: false,
  underscored: false
}});
connection
  .authenticate()
  .then(() => {
    console.log("Conectado com sucesso na base de dados!");
  })
  .catch((error) => {
    console.log(
      "falha ao conectar na base de dados:",
      error.original
    );
  });
const Usuario = require("../models/Usuario");
const Evento = require("../models/Evento");
const Participant = require("../models/Participante")

Usuario.init(connection);
Evento.init(connection);
Participant.init(connection);

module.exports = connection;