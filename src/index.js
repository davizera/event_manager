require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
require("./database/index");

const app = express();
const port = process.env.PORT || 3003;
app.use(express.json());

// routes
const usersRoute = require("./routes/user");
const EventsRoute = require("./routes/event");
const ParticipantsRoute = require("./routes/participant")

app.use(usersRoute);
app.use(EventsRoute);
app.use(ParticipantsRoute);
//Rota principal da aplicacao, para acesso direto na porta
// app.use((_, res, next) => {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
//   app.use(cors());
//   next();
// });
app.get("/", (req, res) => {
  res.send(
    "Ola, esta aplicacao esta rodando em NodeJS versao " + process.version
  );
});
// middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.listen(port);

module.exports = app;
