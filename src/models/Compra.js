const { Model, DataTypes } = require('sequelize');

class Compra extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type:DataTypes.INTEGER,
        primaryKey: true,
        allowNull:false,
        autoIncrement: true
      },
      id_usuario:{
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'usuarios',
          key: 'id'
        }
      },
      valor: {
        type: DataTypes.FLOAT,
        allowNull: false
      },
      quantidade: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false 
      }
    }, {
      sequelize,
      tableName:'compras'
    })
  }
}

module.exports = Compra;