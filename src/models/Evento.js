const { Model, DataTypes } = require('sequelize');

class Evento extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type:DataTypes.INTEGER,
        primaryKey: true,
        allowNull:false,
        autoIncrement: true
      },
      descricao: {
        type: DataTypes.STRING,
        allowNull: false
      },
      nome: {
        type: DataTypes.STRING,
        allowNull: false
      },
      data:{ 
        type: DataTypes.DATE,
        allowNull: false
      },
      imagem: {
        type: DataTypes.BLOB,
        allowNull: true,
        defaultValue: null
      },
      privado: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false
      },
      gratuito: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false
      },
      endereco: {
        type: DataTypes.STRING,
        allowNull: false
      },
      id_usuario: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'usuarios',
          key: 'id'
        }
      },
      valor: {
        type: DataTypes.FLOAT,
        allowNull: true,
        defaultValue: null
      }
    }, {
      sequelize,
      tableName:'eventos'
    })
  }
}

module.exports = Evento;