const User = require('./Usuario') 
const { Model, DataTypes } = require('sequelize');

class Ingresso extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type:DataTypes.INTEGER,
        primaryKey: true,
        allowNull:false,
        autoIncrement: true
      },
      id_evento:{
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'eventos',
          key: 'id'
        }
      },
      id_compra:{
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'compras',
          key: 'id'
        }
      },
      descricao:{
        type: DataTypes.STRING,
        allowNull: false
      },
      valor: {
        type: DataTypes.FLOAT,
        allowNull: false
      },
      quantidade: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false 
      }
    }, {
      sequelize,
      tableName:'ingressos'
    })
  }
}

module.exports = Ingresso;