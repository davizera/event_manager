// const User = require('./Usuario') 
const { Model, DataTypes } = require('sequelize');

class Participante extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type:DataTypes.INTEGER,
        primaryKey: true,
        allowNull:false,
        autoIncrement: true
      },
      id_evento:{
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'eventos',
          key: 'id'
        }
      },
      id_usuario:{
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'usuarios',
          key: 'id'
        }
      },
      id_ingresso: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'ingressos',
          key: 'id'
        }
      },
      nome: {
        type: DataTypes.STRING,
        allowNull: false
      }
    }, {
      sequelize,
      tableName:'participantes'
    })
  }
}

module.exports = Participante;