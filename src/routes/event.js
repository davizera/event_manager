const express = require("express");
const EventController = require("../Controllers/EventController");
const routes = express.Router();
const multer = require("multer");
const path = require("path");
const withAuth = require ('../middlewares/auth')
const diskStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/img/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});
// const diskStorage = require("../utils/file-storage");
const upload = multer({ storage: diskStorage });
routes.post(
  "/event/create",
  upload.single("imagem"),
  EventController.createEvent
);
routes.put("/event/edit/:id_evento", withAuth, upload.single("imagem"), EventController.editEvent);
routes.get("/event", EventController.getEvents);
routes.delete("/event/delete/:id_evento", withAuth,EventController.deleteEventById);
routes.get("/event/:id_usuario", withAuth,EventController.getEventByUserId);
routes.get("/event/one/:id_event", withAuth, EventController.getEventById);
routes.get("/event/search/ev", withAuth, EventController.searchEvent);

module.exports = routes;
