const express = require("express");
const ParticipantController = require("../Controllers/ParticipantController");
const routes = express.Router();

routes.post("/participant/create/:id_usuario/:id_evento", ParticipantController.createParticipant);
// routes.delete("/event/delete/:id_evento", ParticipantController.deleteParticipantesByEventId);
routes.get("/participant", ParticipantController.getParticipantes)

module.exports = routes;
