const express = require('express')
const UserController = require('../Controllers/UserController')
const routes = express.Router()

routes.post('/user/signup', UserController.Register)
routes.post('/user/signin', UserController.Login)
// routes.get('/user', userController.List)
// routes.put('/user/edit/:id', userController.Edit)

module.exports = routes